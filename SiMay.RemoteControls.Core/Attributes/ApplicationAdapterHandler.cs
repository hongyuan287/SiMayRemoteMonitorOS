﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMay.RemoteControls.Core
{
    /// <summary>
    /// 应用处理器属性字段
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class ApplicationAdapterHandlerAttribute : Attribute
    {

    }
}
